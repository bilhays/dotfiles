#!/bin/bash
targethost=$1;
rkey="~/.rootkeys/root_id_rsa"

ssh -i ${rkey}  root@${targethost} "ls -la /Users/kiosk/Library/Preferences/edu.northcarolinastateuniversity.webXkioskII.plist";
ssh -i ${rkey}  root@${targethost} "rm /Users/kiosk/Library/Preferences/edu.northcarolinastateuniversity.webXkioskII*";
scp -i ${rkey} ~/edu.northcarolinastateuniversity.webXkioskII.plist root@${targethost}:/Users/kiosk/Library/Preferences/;
ssh -i ${rkey}  root@${targethost} "chown kiosk  /Users/kiosk/Library/Preferences/edu.northcarolinastateuniversity.webXkioskII.plist";
ssh -i ${rkey}  root@${targethost} "ls -la /Users/kiosk/Library/Preferences/edu*";
ssh -i ${rkey}  root@${targethost} "ps -A | grep webX"
echo "ssh -i ${rkey}  root@${targethost} kill -15 "
echo "ssh -i ${rkey} -L 5903:localhost:5900 root@${targethost}";


# then add pushing the kiosk hashed password for auto login.
