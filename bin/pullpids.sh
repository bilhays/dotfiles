#!/bin/bash

#
# Author:
# hays 18 Oct 2017
#
version="0.1";
# Copyright 2017 by bil hays

# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#
# CAVEAT: Please do not assume that I know what I am doing. Often I don't. Do
# not depend on me or my code to do the expected or desired, we are liable to 
# disappoint you. We take no responsibilty for anything the might happen 
# to you or those around you should you use this software.
#
PROG="${0##*/}";
#PROG="${PROG%.new}";
#PROG="${PROG%.sh}";
#PROG="${PROG%.bash}";

function usage()
   {
cat <<EOF
n no argument required
v version
r argument required

EOF
    }

function check_errm
   {
   if  [[ ${?} != "0" ]]
      then
      echo "${1}";
      exit ${2};
      fi  
   }

function check_file
   {
   if [[ -f ${1} ]]
      then 
	  echo "${1} exists, proceeding...."
   else
      echo "${1} not present, we're dying here...."
	  exit 10;
   fi
   }

function fin () 
   {
   echo "   Fini avec ${PROG}";
   echo "****************************************";
   echo " ";
   }

while getopts ":nor:" Option
do
  case $Option in
    n     ) #no argument required
       q=yes;;
    v  ) 
       echo ${version};;
    r  ) #argument required
       OUTPUT_IS=${OPTARG};;
    h  ) # provide usage
       usage;;   
    *  ) 
       echo "Unimplemented option chosen.";
       usage;
       ;;   # DEFAULT
   esac
done

for j in $(cat ./pids.tab)    
do
    curl http://elektra.cs.unc.edu/ugrad-dir/index2.php?pid="$j";
done


#fin;
