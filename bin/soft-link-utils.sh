#!/bin/bash
# from https://github.com/clburlison/scripts/blob/master/clburlison_scripts/create_links_utilities/mavericks.sh
# ==============================================
# Make links to useful apps - Generic
# ==============================================

# Directory Utility
ln -s "/System/Library/CoreServices/Applications/Directory Utility.app" "/Applications/Utilities/Directory Utility.app"

# Screen Sharing
ln -s "/System/Library/CoreServices/Applications/Screen Sharing.app" "/Applications/Utilities/Screen Sharing.app"

# Ticket Viewer
ln -s "/System/Library/CoreServices/Ticket Viewer.app" "/Applications/Utilities/Ticket Viewer.app"

# Microsoft Office Updater
ln -s "/Library/Application Support/Microsoft/MAU2.0/Microsoft AutoUpdate.app" "/Applications/Utilities/Microsoft AutoUpdate.app"