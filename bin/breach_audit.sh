/var/log/,
/var/adm/,
/var/spool/
wtmp, who,
last, lastlog, /var/log/secure
arp –an,
route print
netstat –nap (Linux),
netstat –na (Solaris),
lsof –i
more /etc/passwd
more /etc/crontab,
ls /etc/cron.*,
ls /var/at/jobs
more /etc/resolv.conf,
more /etc/hosts
rpm -Va (Linux),
pkgchk (Solaris)
chkconfig --list (Linux),
ls /etc/rc*.d (Solaris),
smf (Solaris 10+)
ps aux (Linux, BSD),
ps -ef (Solaris),
lsof +L1
ls –lat /,
find / -mtime -2d -ls
