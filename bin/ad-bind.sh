#!/bin/bash

if [[ ! $1 ]]
   echo "I need a name"
   exit 5
fi
name=${1}
dsconfigad -a  ${name} -u hays -ou "CN=Computers,DC=cs,DC=unc,DC=edu" -domain cs.unc.edu -localhome enable  -groups "Domain Admins,Enterprise Admins" -alldomains enable
sudo scutil --set ComputerName ${name}
sudo scutil --set LocalHostName ${name}
sudo scutil --set HostName ${name}
