#!/bin/bash

# from http://everythingisgray.com/2014/10/07/clean-up-whitespace-with-find-and-sed/
# there's a real tab in there
# Unclean whitespace in source code makes me sad. The following will remove all trailing
# whitespace in files within the current directory that end with .h or .m, (which makes me
# happy). I’ve only tested this on OS X.
find . -regex '.*\.[mh]$' -exec sed -i '' 's/[ 	]*$//' {} \;

