#!/bin/bash

# This script is designed to run a short set of checks
# on ubuntu systems to see if they have the basic set
# of bits required for securing sensitive data.
# It is just a quick audit and does not subsitute
# for more through checks such as qualys
#
# copyright 2016 bil hays
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software\t Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


version="0.6";
# version 0.5
# -added summary message
# -massaged last qualys login
# version 0.6
# -cleaned up for nrpe
# -added parameter flags

ERRS=0;
PROG="${0##*/}";
PROG="${PROG%.new}";
PROG="${PROG%.sh}";
PROG="${PROG%.bash}";
OS="";

logstocheck='/var/log/authd.log
             /var/log/pffirewall.log
             /var/log/clamav/scan.log
             /var/log/lynis.log
             ';

processestocheck='splunk scep_daemon ';

brew-list='sshguard'

osx-filestocheck='/cs/bin/update_os';
ubu-filestocheck=''
SUMMARY='';

function check-os ()
   {
   if [[ -f "/etc/lsb-release" ]]
      then
      OS="Ubuntu";
   elif [[ -f "/etc/redhat-release" ]]
      then
      OS="Redhat";
   elif  [[ $(uname | grep Darwin) ]]
      then
      OS="Darwin";
   else
      OS="Unk";
   fi
   }


function messenger ()
   {
   if [[ ! ${QUIET} ]]
      then
      echo ${1};
   fi
   #echo $ERRS;
   if [[ ${SUMMARY} ]]
      then
      SUMMARY="${SUMMARY}; ${1}";
   else
      SUMMARY="${1}";
   fi
   }

#####
# This checks for an error and exits with a custom message
# Returns zero on success
# $1 is the message
# $2 is the error code

function check_errm
   {
   if  [[ ${?} != "0" ]]
      then
      echo "${1}";
      exit ${2};
      fi
   }

# Tells the user how the program expects to run
function usage
   {
   echo "";
   echo "Usage: ${PROGRAM} [OPTIONS]" >&2;
   echo "   -v = Echo the version number" >&2;
   echo "   -u = set the qualys user id" >&2;
   echo "   -c = Set path to antivirus program" >&2;
   echo "   -l = Set path to splunkforwarder log" >&2;
   echo "   -i = quiet, use for nrpe" >&2;
   echo;

   }

function brew-check
   {
   /usr/local/bin/brew list ${1};
   if [[ ${?}==0 ]]
      then
      messenger "brew ${1} exists";
      return 0;
   else
      messenger "brew ${1} not found";
      ERRS=$((ERRS+1));
      return 1;
   fi
   }


function ubuntu-update-check
   {
   # This code stolen from Murray Andereggg
   if LOG=$(/bin/mktemp /tmp/${PROG}.log.XXXXXXXXXX);
   then
     :
   else
     check_errm "Unable to create temp logfile /tmp/${PROG}.log";
     exit 1;
   fi;

   if APTGETOUT=$(/bin/mktemp /tmp/${PROG}.apt-get.XXXXXXXXXX);
   then
     :
   else
     check_errm "Unable to create working file for apt-get output";
     exit 2;
   fi;

   # Append APTGETOUT to TMPFILES
   TMPFILES="${TMPFILES}${TMPFILES:+ }${APTGETOUT}";

   /usr/bin/apt-get --assume-no upgrade 2>&1 | \
   /bin/sed -ne '/^[       ]/p' | \
   /bin/sed -e 's/^[       ]*//' &> ${APTGETOUT};
   if [ -s "${APTGETOUT}" ];
   then
     number=$(wc -w ${APTGETOUT} | awk '{ print $1; }');
     number=${number};
     first=$(cat ${APTGETOUT} | tr '\012' ' ' | cut -c-40);
     case ${number} in
       1)
         messenger  "***WARNING: 1 update: ${first}";
         ;;
       *)
         messenger  "***WARNING: ${number} updates: ${first}";
         ;;
     esac;
     ERRS=$((ERRS+1));
   else
     messenger  "No security updates required";
   fi;
   }

function osx-update-check ()
   {
   COMMAND_LINE_INSTALL=1;
   export COMMAND_LINE_INSTALL;
   # Run the cli version of software update
   #echo ${1};
   results=$(/usr/sbin/softwareupdate -l) > /dev/null;
   echo ${results} | grep "recommended" > /dev/null;
   if [[ ${?} == 0 ]] ;
      then
      messenger "WARNING: ${hostname} OS Needs updating";
      ERRS=$((ERRS+1));
   else
      messenger "${hostname} OS is up to date";

   fi
   }

function check-log ()
   {
   # Check to see if the log is stale
   if [[ -f ${1} ]]
      then
      if [[ $(find ${1} -mtime +7 ) ]]
         then
         messenger  "***WARNING: ${1} is older than seven days";
         ERRS=$((ERRS+1));
      else
         messenger  "${1} updated in the last seven days"
      fi
   else
      messenger  "***WARNING: ${1} not found.";
      ERRS=$((ERRS+1));
   fi
   }


function check-process ()
   {
   # Check a running process.
   if [[ $(pgrep ${1}) ]]
      then
      messenger  "${1} is running";
   else
      messenger  "***WARNING: ${1} is not running";
      ERRS=$((ERRS+1));
   fi
   }

function check-file ()
   {
   # check for a file
   if [[ -f ${1} ]]
      then
      messenger  "We have ${1}";
   else
      messenger  "***WARNING: we have no ${1}";
      ERRS=$((ERRS+1))
   fi
   }


while getopts "vu:c:l:q" Option
do
  case $Option in
    v     ) echo;
            echo "   Version is ${version}";
            echo;
            exit;;
    u     ) #echo "   Using ${OPTARG} as as qualys user";
          QUALYSUSER=${OPTARG};;
    q     ) # quiet mode
          QUIET="1";;
    *     )
          echo "Unimplemented option chosen.";
          usage;
          ;;   # DEFAULT
   esac
done

# Figure out where we are
check-os;

for fn in ${filestocheck};
   do
   check-file ${fn};
   done

for pn in ${processestocheck};
   do
   check-process ${pn};
   done

for ln in ${logstocheck};
   do
   check-log ${ln};
   done

if [[ $OS == "Darwin" ]]
   then
   for br in ${brew-list};
      do
         check-process ${br};
      done
fi


if [[ $OS == "Darwin" ]]
   then
   QUALYSHOME="/Users/";
   oscheck="osx-update-check";
else
   QUALYSHOME="/home/";
   oscheck="ubuntu-update-check";
fi

if [[ ! ${QUALYSUSER} ]]
   then
   messenger "No qualys user specified";
   ERRS=$((ERRS+1));
else
   # check for qualys user's home dir
   echo "${QUALYSHOME}${QUALYSUSER}";
   if [[ -d "${QUALYSHOME}${QUALYSUSER}" ]]
      then
      messenger  "${QUALYSUSER} home dir found";
      # check for keys for the qualys user
      if [[ $(grep "${QUALYSUSER}" "${QUALYSHOME}${QUALYSUSER}/.ssh/authorized_keys" 2>&1) ]]
         then
         messenger  "${QUALYSUSER} found in authorized_keys";
      else
         messenger  "***WARNING: ${QUALYSUSER} user not found in authorized_keys";
         ERRS=$((ERRS+1))
      fi

      if [[ $(last -n 1 ${QUALYSUSER} | grep ${QUALYSUSER}) ]]
         then
         messenger "${QUALYSUSER} has logged in";
         messenger $(last -n 1 ${QUALYSUSER});
      else
          messenger "***${QUALYSUSER} has not logged in";
          ERRS=$((ERRS+1));
      fi
   fi
fi


# check for security system updates
echo "Checking for system updates, this may take a bit";
${oscheck};



# If we have had errors, exit with error coded and a count.
if [[ ${ERRS} > 1 ]]
   then
   echo "${ERRS} issues found!";
   exit ${ERRS};
elif [[ ${ERRS} > 0 ]]
   then
   echo "${ERRS} issue found!";
   exit 1;
else
    echo "No issues found";
    exit 0;
fi
