#!/bin/bash
version="0.9"; 
# Copyright (c) bil hays, 2013
#
# This script will read a file containing hostnames or ip addresses
# ping them, and read the list of live machines to the host.list file
# 
#This program is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.

#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.

#You should have received a copy of the GNU General Public License
#along with this program.  If not, see #<http://www.gnu.org/licenses/>.


filelist=${1}
rm host.list
rm deadhost.list
while read line        
do  
    x=$(host ${line});      
    ping -c 3 ${line};
    if [[ ${?} == 0 ]]
       then 
       echo  "${x##*' '}:\t${line}" >> host.list
    else
       echo "${x##*' '}:\t${line}" >> deadhost.list
    fi
done < "${filelist}" 

echo;
echo "The following machines weren't alive:";
cat deadhost.list;

