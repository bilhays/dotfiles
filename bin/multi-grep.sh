#!/bin/bash

#
# Author:
# hays 18 Oct 2017
#
version="0.1";
# Copyright 2017 by bil hays

# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#
# CAVEAT: Please do not assume that I know what I am doing. Often I don't. Do
# not depend on me or my code to do the expected or desired, we are liable to 
# disappoint you. We take no responsibilty for anything the might happen 
# to you or those around you should you use this software.
#
PROG="${0##*/}";
#PROG="${PROG%.new}";
#PROG="${PROG%.sh}";
#PROG="${PROG%.bash}";

function usage()
   {
cat <<EOF
-v Version
-s String to search for
-t Type of search:
   m greps nethosts over ssh
   n greps all.txt with curl
   u greps userdb over ssh
   p runs 411 on the string
   l greps user.list with curl
EOF
    }

function check_errm
   {
   if  [[ ${?} != "0" ]]
      then
      echo "${1}";
      exit ${2};
      fi  
   }

function check_file
   {
   if [[ -f ${1} ]]
      then 
	  echo "${1} exists, proceeding...."
   else
      echo "${1} not present, we're dying here...."
	  exit 10;
   fi
   }

function fin () 
   {
   echo "   Fini avec ${PROG}";
   echo "****************************************";
   echo " ";
   }

function lgrep ()
   {
   curl -u ${USER} -s https://www.cs.unc.edu/xhelp/user-info/user.list | grep ${1} | tr -s [:space:] ' '
   }

# ugrep sshs a command to search the userdb file
function ugrep
   {
   ssh fafnir "/var/adm/util/user/ugrep ${1}";
   }

# mgrep sshs a command to search the userdb file
function mgrep
   {
   if [[ ! -f ~/.rootkeys/root_id_rsa ]]
      then
      rootkeys;
      sleep 2;
   fi
   ssh -i ~/.rootkeys/root_id_rsa root@bristol.cs.unc.edu "grep ${1}  /var/named/hosts/nethosts";
   }
   
function ngrep
   {
   curl https://www.cs.unc.edu/xhelp/dept_only/locations/all.txt | grep ${1}
   }   


while getopts ":nt:s:" Option
do
  case $Option in
    v  ) 
       echo ${version};;
    t  ) #argument required
       TYPE=${OPTARG};;
    s  ) #argument required
       STRING=${OPTARG};;
    h  ) # provide usage
       usage;;   
    *  ) 
       echo "Unimplemented option chosen.";
       usage;
       ;;   # DEFAULT
   esac
done


case ${TYPE} in
    l )
        echo "grep user.list for ${STRING}";
        lgrep ${STRING};
        ;;
    m )
        echo "grep nethosts for ${STRING}";
        mgrep ${STRING};
        ;;
    n ) echo "grep all.txt for ${STRING}";
        ngrep ${STRING};    
    u )
        echo "grep userdb for ${STRING}"
        ugrep "${STRING}";
        ;;
    p )
        echo "grep whitepages for ${STRING}"
        411 "${STRING}";
        ;;   
              
esac

exit



fin;
