#!/bin/sh

whoami=`/usr/ucb/whoami`
if [ "$whoami" != root ]; then
        echo ""
        echo "You must be root to run ${0}."
        echo ""
        exit 1
fi

if [ ! -h /dev/hme ]; then
	exit 0
fi

# set interface:

ndd -set /dev/hme instance 0

# get speed:

speed=`ndd -get /dev/hme link_speed`

if [ "$speed" -eq 0 ]; then
	echo "hme speed => 10Mbs"
else
	echo "hme speed => 100Mbs"
fi

# get full/half duplex mode:

mode=`ndd -get /dev/hme link_mode`

if [ $mode -eq 0 ]; then
	echo "hme mode  => Half Duplex"
else
	echo "hme mode  => Full Duplex"
fi
