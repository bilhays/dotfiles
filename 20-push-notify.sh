# From https://gist.github.com/ttscoff/6e041346fd10834c0ae48599b45e9276

# Use the [Pushover](https://pushover.net/) API to send a notice to your phone/watch
pushnotify() {
	local token=xxxxxxxxxxxxxxxxxxx
	local key=xxxxxxxxxxxxxxxxxxx
	local formdata="token=${token}&user=${key}&title=$(urlenc $1)&message=$(urlenc $2)&priority=1&sound=spacealarm"
	curl -X POST -d $formdata 'https://api.pushover.net/1/messages.json'
}

