# Larger bash history (default is 500)
export HISTFILESIZE=10000
export HISTSIZE=10000
#
source /Users/hays/dotfiles/source/10-keys.sh

source ~/dotfiles/bin/dotfiles

test -e "${HOME}/.iterm2_shell_integration.bash" && source "${HOME}/.iterm2_shell_integration.bash"

test -e "${HOME}/dotfiles/_bashfunctions.sh"  && source "${HOME}/dotfiles/_bashfunctions.sh"

test -e "${HOME}/dotfiles/_environment.sh"  && source "${HOME}/dotfiles/_environment.sh"
