# From https://gist.github.com/ttscoff/6e041346fd10834c0ae48599b45e9276

# Poll a DNS resolver to see when you can reach it, then notify
imdown() {
	until ping -W1 -c1 8.8.8.8; do
		sleep 5;
	done
	nag \
	"internet connection is back up\!" \
	"Skynet is thinking" "your tribulation is over\!" \
	"Praise what gods may be. internet\!" \
	"O M G we're online" \
	"In the words of Dr. Frankenstein, it's alive\!"
}
