# OSX-only stuff. Abort if not OSX.
if [[ $(is_osx) ]]
   then
   export PATH=~/dotfiles/bin:/usr/local/Cellar/openssl/1.0.2l/bin:/cs/bin:/cs/sbin:/usr/local/bin:/usr/local/sbin:/opt/local/bin:/opt/local/sbin:/opt/X11/bin:/usr/local/MacGPG2/bin:/usr/local/opt/go/libexec/bin:/usr/bin:/usr/sbin:/bin:/sbin
fi

