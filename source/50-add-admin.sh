#!/bin/bash

#
# add-admin.sh
#
# Defines functions addadmin and deladmin for adding admin privileges
# for a user and deleting admin privileges for a user.
#
# This does *not* work on Mac OS X, so the functions will not be defined.
#
# Anderegg, October 2, 2018
#
# Set PROG for messages, only if not defined.
[ -z "${PROG}" ] && { \
  PROG=${0##*/};
  PROG=${PROG%.new};
  PROG=${PROG%.bash};
  PROG=${PROG%.sh};
};

# Utility function check and basic defines:
# stderr - Send message to stderr
# error - Send error message to stderr
# have_terminal - Is stdout connected to a terminal?
# terminal - Send message to terminal, only if connected.
#
type -t stderr &> /dev/null || \
  stderr () {
    echo "$@" >&2;
  }

type -t error &> /dev/null || \
  error () {
    stderr "${PROG}: error: $@";
  }

type -t have_terminal &> /dev/null || \
  have_terminal () {
    [ -t 1 ];
  }

# Bail out if on a Mac.
case $(uname) in
  Darwin)
    error "addadmin() and deladmin() do not work on Mac OS X";
    return 0 &> /dev/null;
    exit 0;
    ;;
esac;

__aa_find_cmd () {
  local cmd="$1" var="$2" p=;
  shift 2;
  type stderr &> /dev/null || function stderr ()
    {
      echo "$@" 1>&2
    };
  [ -z "${var}" ] && stderr "No assignment variable given." && return 1;
  [ -z "${cmd}" ] && stderr "No command to search for given." && return 2;
  for p in "$@";
  do
    [ -x "${p}"/"${cmd}" -a "${var}" != '-' ] && eval "${var}=${p}/${cmd}" && return 0;
    [ -x "${p}"/"${cmd}" -a "${var}" = '-' ] && echo "${p}/${cmd}" && return 0;
  done;
  stderr "Unable to find command: ${cmd}";
  eval "${var}=false";
  return 3;
}

__aa_check_prereq_vars () {
  local v= varlist="$@" rc=0;
  local missing=;
  for v in ${varlist};
  do
    eval [ -n \"\$${v}\" ] || {
      missing="${missing}${missing:+, }${v}";
    rc=2
  };
  done;
  if [ ${rc} -ne 0 ];
  then
    error "All variables in list must be set:";
    stderr "  ${varlist// /, }";
    stderr "The following variables were not set:";
    stderr "  ${missing}";
    return ${rc};
  fi;
  return 0
}

__aa_find_cmd awk _AA_AWK /bin /usr/bin;
__aa_find_cmd grep _AA_GREP /bin /usr/bin;
__aa_find_cmd mktemp _AA_MKTEMP /bin /usr/bin;

__aa_check_prereq_vars _AA_AWK _AA_GREP _AA_MKTEMP;

#
# create_sudoers_d()
# Create an entry in /etc/sudoers.d for the requested entity.
# For a group the entity *must* begin with a %, or else
# it will appear to be a user.
#
__aa_create_sudoers_d () {
  local u="$1" usrsudo=;

  # A group must begin with a %, but the filename does not.
  case "${u}" in
    %*)
      usrsudo="${u#%}";
      ;;
    *)
      usrsudo="${u}";
      ;;
  esac;

  # Special case two facil "Computer Services" people, since they
  # are in a group file, which sorts after their userid's.
  case "${usrsudo}" in
    anderegg|dcowhig)
      usrsudo="zzz-${usrsudo}";
      ;;
    *)
      ;;
  esac;

  # Create temporary sudoers.d file.
  have_terminal && echo "Create temporary ${u} sudoers.d file";
  if sudotmp=$(${_AA_MKTEMP} /tmp/sudoers.d.${u}.XXXXXXXXXX);
  then
    chown root:root ${sudotmp};
    cat > ${sudotmp} << E_O_USRSUDO
${u}  ALL=(ALL) ALL
E_O_USRSUDO

    # Set permissions, then install sudoers.d file.
    chmod ug=r,o= ${sudotmp};
    have_terminal && echo "Install ${u} sudoers.d file";
    /bin/mv ${sudotmp} /etc/sudoers.d/${usrsudo};
  else
    error "Unable to create temporary ${u} sudoers.d file";
    return 1;
  fi;
}

#
# Remove an /etc/sudoers.d file for a user or group.
#
__aa_remove_sudoers_d () {
  local usrsudo="$1";

  # Special case two facil "Computer Services" people, since they
  # are in a group file, which sorts after their userid's.
  case "${usrsudo}" in
    anderegg|dcowhig)
      usrsudo="zzz-${usrsudo}";
      ;;
    *)
      ;;
  esac;

  # Trim leading % from group name to generate filename.
  usrsudo="${usrsudo#%}";

  /bin/rm -f /etc/sudoers.d/"${usrsudo}";
}

#
# addadmin
#
# Add admin privileges for a non-system user.
# Check if user exists.
# Check if user's UID is below UID_MIN with default 1000 for Linux, 500 for Mac.
#
#
addadmin () {
  local u= uid= uidmin= g= usrsudo=;
  local sudogroups="adm admin sudo" sudotmp=;

  # Utility function for checking if a user exists.
  usrchk () {
    local u="$1";

    getent passwd ${u} &> /dev/null;
  }

  # Utility function for checking if a group exists.
  grpchk () {
    local g="$1";

    ${_AA_GREP} -q "^${g}:" /etc/group;
  }

  # Utility function for obtaining minimum user account UID.
  sysuidmin () {
    local uidmin=;

    # Attempt to get lower bound from system.
    # On failure, set default.
    uidmin=$(${_AA_AWK} '$1 == "UID_MIN" { print $2; }' /etc/login.defs 2> /dev/null);
    case "$(uname)" in
      Darwin)
        uidmin=${uidmin:-500};
        ;;
      Linux)
        uidmin=${uidmin:-1000};
        ;;
    esac;
    echo ${uidmin};
  }
  uidmin=$(sysuidmin);

  # For each user given on the commandline, attempt to
  # add the user to the sudo groups and create an entry
  # in /etc/sudoers.d for the user.
  for u in "$@";
  do
    usrchk ${u} || \
      { error "Unable to find user ${u}. Skipping"; \
        continue; }
    uid=$(getent passwd ${u} | ${_AA_AWK} -F: '{ print $3; }');
    [ ${uid} -lt  ${uidmin} ] && \
      error "Can not alter admin groups on system account ${u}" && \
      continue;
    for g in ${sudogroups};
    do
      if grpchk ${g};
      then
        have_terminal && echo "Add ${u} to group ${g}";
        /usr/sbin/usermod -a -G ${g} ${u};
      fi;
    done;

    __aa_create_sudoers_d "${u}";
  done;

  unset usrchk grpchk sysuidmin;
}

#
# deladmin
#
# Delete admin privileges for the list of users.
# For each user:
# - Check whether user exists.
# - Check whether user is a system user.
# - Remove user from each sudo group.
# - Remove user's sudoers.d file.
# - Remove user from /etc/sudoers.
#
deladmin () {
  local u= uid= uidmin= g= grplist=;
  local sudogroups="adm admin sudo";

  # Utility function to check whether user exists.
  usrchk () {
    local u="$1";

    getent passwd ${u} &> /dev/null;
  }

  # Utility function to check whether a group exists.
  grpchk () {
    local g="$1";

    ${_AA_GREP} -q "^${g}:" /etc/group;
  }

  # Utility function for obtaining minimum user account UID.
  sysuidmin () {
    local uidmin=;

    # Attempt to get lower bound from system.
    # On failure, set default.
    uidmin=$(${_AA_AWK} '$1 == "UID_MIN" { print $2; }' /etc/login.defs 2> /dev/null);
    case "$(uname)" in
      Darwin)
        uidmin=${uidmin:-500};
        ;;
      Linux)
        uidmin=${uidmin:-1000};
        ;;
    esac;
    echo ${uidmin};
  }
  uidmin=$(sysuidmin);

  # For each user requested:
  # - Check whether user exists.
  # - Check whether user's UID is a system UID.
  # - For each sudogroup, remove the user from the group.
  # - Remove the user's sudoers.d file.
  # - Remove the user from /etc/sudoers.
  #
  for u in "$@";
  do
    usrchk ${u} || \
      { error "Unable to find user ${u}. Skipping"; \
        continue; }
    uid=$(getent passwd ${u} | ${_AA_AWK} -F: '{ print $3; }');
    [ ${uid} -lt  ${uidmin} ] && \
      error "Can not remove sudo from system account ${u}" && \
      continue;

    for g in adm admin sudo;
    do
      if grpchk ${g};
      then
        have_terminal && echo "Remove ${u} from group ${g}";
        /usr/bin/gpasswd --delete ${u} ${g};
      fi;
    done;

    # Remove the sudoers.d file for user ${u}.
    __aa_remove_sudoers_d "${u}";

    # Check if user was added to /etc/sudoers, and remove
    # if needed.
    if ${_AA_GREP} -qE "^${u}[[:space:]]" /etc/sudoers;
    then
      have_terminal && echo "Remove ${u} from /etc/sudoers";
      flock /etc/sudoers -c "/bin/sed -i -e \"/^${u}[ 	]/d\" /etc/sudoers";
    fi;
  done;

  unset usrchk grpchk sysuidmin;
}

#
# Add an admin group.
# Check if group exists in /etc/group, unless override.
# Only add group to /etc/sudoers.d.
#
addadmingrp () {
  local over='no' grp=;

  # Unless override is declared, ensure that the group exists.
  case "$1" in
    -o|--over|--override)
      over='yes';
      shift;
      ;;
    *)
      ;;
  esac;

  #
  # Walk through each requested group and create an /etc/sudoers.d file
  # for the group.
  #
  for grp in "$@";
  do
    case "${over}" in
      yes)
        ;;
      *)
        /usr/bin/getent group "${grp}" || \
          { error "Unable to find group ${grp}. Skipping"; \
            continue; }
        ;;
    esac;

    if [ "${grp}" = "${grp#%}" ];
    then
      __aa_create_sudoers_d "%${grp}";
    else
      __aa_create_sudoers_d "${grp}";
    fi;
  done;
}

#
# Delete an admin group.
#
deladmingrp () {
  local grp=;

  for grp in "$@";
  do
    if [ "${grp}" = "${grp#%}" ];
    then
      __aa_remove_sudoers_d "%${grp}";
    else
      __aa_remove_sudoers_d "${grp}";
    fi;
  done;
}

#
# Allow script to be a standalone.
#
case ${PROG} in
  addadmin|add-admin)
    addadmin "$@";
    ;;
  deladmin|del-admin)
    deladmin "$@";
    ;;
  addadmingrp)
    addadmingrp "$@";
    ;;
  deladmingrp)
    deladmingrp "$@";
    ;;
esac;
# vim: set ft=sh et sw=2 sts=2 ts=2 :
