# OSX-only stuff. Abort if not OSX.
if [[ $(is_osx) ]]
   then
   # reloadld reloads a laundaemon
   function reloadl()
      {
      launchctl stop ${1};
      check_errm "stop ${1} failed" 1
      launchctl unload /Library/LaunchDaemons/${1}.plist;
      check_errm "unload ${1}.plist failed" 2
      launchctl load /Library/LaunchDaemons/${1}.plist;
      check_errm "load ${1}.plist failed" 3
      launchctl start ${1};
      check_errm "start ${1} failed" 4
      }
fi