# from http://mah.everybody.org/docs/ssh
function start_agent {

if [[ -f "/usr/bin/ssh-agent" ]]
    then
    SSHAGENT=/usr/bin/ssh-agent
 elif [[ -f "/usr/local/bin/ssh-agent" ]]
    then
    SSHAGENT="/usr/local/bin/ssh-agent";
else
    echo "Can't find the agent";
    exit 1;
fi

SSHAGENTARGS="-s"
if [ -z "$SSH_AUTH_SOCK" -a -x "$SSHAGENT" ];
   then
   eval `$SSHAGENT $SSHAGENTARGS`
   trap "kill $SSH_AGENT_PID" 0
fi
}

function keys ()
   {
   if [[ ! -f "${HOME}/.ssh-private/id_rsa" ]]
      then
      encfs -i 15 ${HOME}/.ssh-crypt ${HOME}/.ssh-private
      fi
   }

function ukeys ()
   {
   umount ${HOME}/.ssh-private
   }

function rootkeys ()
   {
   if [[ ! -f "${HOME}/.rootkeys/root_id_rsa" ]]
      then
      encfs -i 15 ${HOME}/.rootkeys-crypt ${HOME}/.rootkeys
      fi
   }

function urootkeys ()
   {
   umount ${HOME}/.rootkeys
   }

function sssh ()
   {
   KEYFILE="${HOME}/.ssh-private/id_rsa"
   GITKEYFILE="${HOME}/.ssh-private/id_rsa-git"
   # Check to see if the vault is already open
   if [[ ! -f ${KEYFILE} ]]
      then
      echo "Opening vault.";
      # The keys function opens the vault
      keys;
   else
      echo "Vault is already open.";
   fi

   if [[ ${?} != 0 ]]
      then
      echo "Couldn't open vault, dying here...";
      return 5;
   fi

   # Start the agent
   start_agent;
   if [[ ${?} != 0 ]]
      then
      echo "Couldn't start the ssh-agent...";
      return 10;
   fi

   echo "Adding key to ssh-agent.";
   sleep 2;
   ssh-add -t 4h "${KEYFILE}";
   if [[ ${?} == 0 ]]
      then
      echo "key added";
   else
      echo "Failed to add key";
      return 15;
   fi

   echo "Adding git key to ssh-agent.";
   sleep 2;
   ssh-add -t 4h "${GITKEYFILE}";
   if [[ ${?} == 0 ]]
      then
      echo "git key added";
   else
      echo "Failed to add git key";
      return 15;
   fi

   ukeys
   }



function sshro ()
   {
   if [[ ! -f ~/.rootkeys/root_id_rsa ]]
      then
      rootkeys;
      sleep 2;
   fi
      ssh -i ~/.rootkeys/root_id_rsa root@${1}
   }


function sshr ()
   {
   if [[ ! -f ~/.rootkeys/root-2019-rsa ]]
      then
      rootkeys;
      sleep 2;
   fi
      ssh -i ~/.rootkeys/root-2019-rsa root@${1}
   }
