# invoke
#
# Shamelessly pilfered from dkms...
# 10 July 2014
#
# $1 = command to be executed using eval.
# $2 = Description of command to run
# $3 = 'background' if you want to run the command asynchronously, -or-
# $3 = 'log' to only see command output, when command fails.
# Ubuntu stuff. Abort if not Ubuntu desktop.
if [[ $(is_ubuntu) ]]
   then
   invoke ()
   {
       local die=no succ="ok" fail="Failed";
       # Introduce notion of dying on failure...
       case "$1" in
         [Dd][Ii][Ee])
           die="yes";
           shift;
           ;;
       esac;

       # Success outputs 'succ', and failure outputs 'fail'
       case "$1" in
         [Oo][Kk])
           succ="ok";
           fail="Failed";
           shift;
           ;;
         [Ff][Aa][Ii][Ll]|[Ff][Aa][Ii][Ll][Ee][Dd])
           succ="failed";
           fail="Succeeded";
           shift;
           ;;
         [Yy]|[Yy][Ee][Ss])
           succ="yes";
           fail="No";
           shift;
           ;;
         [Nn]|[Nn][Oo])
           succ="no";
           fail="Yes";
           shift;
           ;;
       esac;

       # $1 = command to be executed using eval.
       # $2 = Description of command to run
       # $3 = 'background' if you want to run the command asynchronously, -or-
       # $3 = 'log' to only see command output, when command fails.
       local exitval=0

       # Change so that if a description is provided, then it is used instead.
       # [[ $verbose ]] && echo -e "$1" || echo -en "$2..."
       if [ -n "$2" ];
       then
           if [ "${2%:}" != "${2}" -o "${2%\?}" != "${2}" ];
           then
             echo -en "$2 ";
           else
             echo -en "${2}: ";
           fi;
       else
           echo -en "${1}: ";
       fi;
       # This used to include && ! verbose
       if [[ $3 = background || $3 = log ]]; then
           local pid progresspid
           (eval "$1" >> ${LOG} 2>&1) & pid=$!
           while [ -d /proc/$pid ]; do
                   sleep 3
                   verbose -n "."
           done & progresspid=$!
           wait $pid 2>/dev/null
           exitval=$?
           kill $progresspid 2>/dev/null
           wait $progresspid 2>/dev/null
           verbose -n " ";
       else
           eval "$1"; exitval=$?
       fi
       case ${exitval} in
         0)
           eval echo -e "\${${VARIANT}_${SUCCESS}}${succ}\${NONE}";
           ;;
   # Use this if you know that a 1 is a warning.
   #      1)
   #        echo -e "${WARNING}Warning${NONE}";
   #        ;;
         *)
           stat=$?;
           eval echo -e "\${${VARIANT}_${FAILURE}}${fail}\${NONE}";
           if [ "${die}" = "yes" ];
           then
             exit ${stat};
           fi;
           ;;
       esac;
       return $exitval
   }
fi