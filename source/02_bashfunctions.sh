#####
#
# Functions
#
# version 0.5
# Much of this work done by Murray Anderegg,
# thanks to him for letting me steal his code
# Copyright 2016 by bil hays

# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#
# CAVEAT: Please do not assume that I know what I am doing. Often I don't. Do
# not depend on me or my code to do the expected or desired, we are liable to
# disappoint you. We take no responsibilty for anything the might happen
# to you or those around you should you use this software.

showdisk ()
{
sudo fdisk -l | egrep 'Disk.*bytes' | awk '{ sub(/,/,""); sum +=$3; print $2" "$3" "$4 } END { print "—————–"; print "total: " sum " GB"}'
}

shadow ()
{
    local u= ids=no;
    local SHADOWHOME="walleye";
    local SPECIALUSERS="root adams";
    case "$1" in
        -i | --id)
            ids=yes;
            shift
        ;;
        -n | --no-id)
            ids=no;
            shift
        ;;
        *)
            if [ $# -gt 1 ]; then
                ids=yes;
            fi
        ;;
    esac;
    for u in "$@";
    do
        case " ${SPECIALUSERS} " in
            *\ ${u}\ *)
                e=$(ssh root@${SHADOWHOME} grep ^${u}: /etc/shadow)
            ;;
            *)
                e=$(ssh root@${SHADOWHOME} grep ^${u}: /root/shadow.md5passwds)
            ;;
        esac;
        case ${ids} in
            yes)
                [ -n "${e}" ] && echo "${u} ${e}"
            ;;
            *)
                [ -n "${e}" ] && echo "${e}"
            ;;
        esac;
    done
}

pwdcrypt ()
{
    local ids=;
    case "$1" in
        -i | --id)
            ids="--id";
            shift
        ;;
        -n | --no-id)
            shift
        ;;
        *)
            if [ $# -gt 1 ]; then
                ids="--id";
            fi
        ;;
    esac;
    case ${ids} in
        --id)
            shadow -n "$@" | awk -F: '{ print $1" "$2; }'
        ;;
        *)
            shadow -n "$@" | awk -F: '{ print $2; }'
        ;;
    esac
}


genuseradd ()
{
    if [[ ! -f ~/.rootkeys/root_id_rsa ]]
       then
       rootkeys;
    fi
    ssh-add -t 4h ~/.rootkeys/root_id_rsa;
    local fmt="command";
    local ent=;
    local usr= usrnam= usrid= usrcmt= usrshell= usrpass=;
    case "$1" in
        -k | --ks)
            fmt="ks";
            shift
        ;;
        -5 | --k5)
            fmt="k5";
            shift
        ;;
    esac;
    for usr in "$@";
    do
        if ent=$(ypmatch ${usr} passwd 2> /dev/null); then
            :;
        else
            if ent=$(grep "^${usr}:" /etc/passwd); then
                :;
            else
                echo "Unable to find user ${usr} in passwd map" 1>&2;
                continue;
            fi;
        fi;
        usrnam=${usr};
        ent=${ent#*:};
        ent=${ent#*:};
        usrid=${ent%%:*};
        ent=${ent#*:};
        ent=${ent#*:};
        usrcmt=${ent%%:*};
        ent=${ent#*:};
        ent=${ent#*:};
        usrshell=${ent};
        usrpass=$(pwdcrypt -n ${usrnam});
        case ${fmt} in
            ks)
                echo user --name=${usrnam} --password=${usrpass} --iscrypted --home=/home/${usrnam} --uid=${usrid} --shell=${usrshell}
            ;;
            k5)
                echo useradd -u ${usrid} -c \"${usrcmt}\" -m -s ${usrshell} ${usrnam}
            ;;
            command)
                echo useradd -u ${usrid} -p \'${usrpass}\' -c \"${usrcmt}\" -m -s ${usrshell} ${usrnam}
            ;;
        esac;
    done
}


# dnsrec finds DNS records matching what's on the command-line.
dnsrec ()
{
    case "$@" in
        *\;*)
            echo "dnsrec(): No semi-colon in grep parameters" 1>&2;
            return 1
        ;;
    esac;
    ssh root@bristol \( cd /var/named/hosts \&\& /bin/grep -vE "\"^[[:space:]]*\#\"" nethosts \| /usr/bin/awk \"\\\$1 !~ /:/\" \| /bin/grep -E "$@" \) 2> /dev/null
}

# canon returns the canonical hostname for any matching DNS records.
canon ()
{
    dnsrec "$@" | /usr/bin/awk '{ print $3; }'
}


function download.sh () {
if [ $# -ne 1 ]; then
  echo "Usage: download.sh file ..."
  exit 1
fi
for fn in "$@"
do
  if [ -r "$fn" ] ; then
    printf '\033]1337;File=name='`echo -n "$fn" | base64`";"
    wc -c "$fn" | awk '{printf "size=%d",$1}'
    printf ":"
    base64 < "$fn"
    printf '\a'
  else
    echo File $fn does not exist or is not readable.
  fi
done
}

# Don't bother typing "cd foo; ls -la" - instead
# add this to your startup script of preference and just do it in one:
# https://coderwall.com/p/kr6sqg
function cl () {
   if [ $# = 0 ]; then
      ls -la
   else
      pushd "$*" && ls -la
   fi
}

# mkdir, cd into it
# from http://onethingwell.org/page/292
mkcd () {
    mkdir -p "$*"
    cd "$*"
}



function lgrep ()
   {
   curl -u ${USER} -s https://www.cs.unc.edu/xhelp/user-info/user.list | grep ${1} | tr -s [:space:] ' '
   }



# ugrep sshs a command to search the userdb file
function ugrep
   {
   ssh bowfin.cs.unc.edu "/var/adm/util/user/ugrep ${1}";
   }


# mgrep sshs a command to search the userdb file
function mgrep
   {
   if [[ ! -f ~/.rootkeys/root_id_rsa ]]
      then
      rootkeys;
      sleep 2;
   fi
   ssh -i ~/.rootkeys/root_id_rsa root@vuelta.cs.unc.edu "grep ${1}  /var/cache/bind/dns-dhcp/nethosts";
   }

