# Copyright 2016 by bil hays

# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#
# CAVEAT: Please do not assume that I know what I am doing. Often I don't. Do
# not depend on me or my code to do the expected or desired, we are liable to
# disappoint you. We take no responsibilty for anything the might happen
# to you or those around you should you use this software.


export PATH=~/dotfiles/bin:/cs/bin:/cs/sbin:/usr/local/bin:/usr/local/sbin:/opt/local/bin:/opt/local/sbin:/usr/bin:/usr/sbin:/bin:/sbin:/opt/X11/bin:/usr/local/MacGPG2/bin

# If I don't have a tgt, might as well get one
if [[ ! $(klist | grep krbtgt) ]]
   then
   kinit hays@CS.UNC.EDU;
   aklog -c CS.UNC.EDU
fi


# go fu
#export PATH=$PATH:/usr/local/opt/go/libexec/bin

export PS1="\[\033[36m\]\u\[\033[m\]@\[\033[32m\]\h:\[\033[33;1m\]\w\[\033[m\]\$ "
alias ls='ls -GFh'

eval $(thefuck --alias)


if [[ -z $DISPLAY && $(sw_vers -productVersion) < 10.5  && -z $SSH_CONNECTION ]]; then
	# -- works for Apple X11 with Fast User Switching
    disp_no=($( ps -awx | grep -F X11.app | awk '{print $NF}' | grep -e ":[0-9]"  ))
    if [[ -n $disp_no ]];then
        export DISPLAY=${disp_no}.0
    else
        export DISPLAY=:0.0
    fi
    echo "DISPLAY has been set to $DISPLAY"
fi

# iTerm Tab and Title Customization and prompt customization

# Put the string " [bash]   hostname::/full/directory/path"
# in the title bar using the command sequence
# \[\e]2;[bash]   \h::\]$PWD\[\a\]

# Put the penultimate and current directory
# in the iterm tab
# \[\e]1;\]$(basename $(dirname $PWD))/\W\[\a\]

# Make a simple command-line prompt:  bash-$

# PS1=$'\[\e]2;[bash]   \h::\]$PWD\[\a\]\[\e]1;\]$(basename "$(dirname "$PWD")")/\W\[\a\]bash-\$ '

alias cd..="cd .."
alias cd...="cd ../.."
alias cd....="cd ../../.."
alias cd.....="cd ../../../.."
alias cd......="cd ../../../../.."

# from https://mobilejazz.com/blog/maintaining-your-sourcetree-bookmarks-throughout-several-machines/
alias sourcetreei="cp ~/Library/Application\ Support/SourceTree/browser.plist ~/Library/Application\ Support/SourceTree/browser.plist.bak;rm ~/Library/Application\ Support/SourceTree/browser.plist; cp ~/Dropbox/shared-config/sourcetree/browser.plist ~/Library/Application\ Support/SourceTree"
alias sourcetreex="cp ~/Dropbox/shared-config/sourcetree/browser.plist ~/Dropbox/shared-config/sourcetree/browser.plist.bak  cp ~/Library/Application\ Support/SourceTree/browser.plist ~/Dropbox/shared-config/sourcetree"
