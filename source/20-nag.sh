# From https://gist.github.com/ttscoff/6e041346fd10834c0ae48599b45e9276

# A truly atrocious way to get your attention
nag() {
	while true; do
		for phrase in "$@"; do
			afplay /System/Library/Sounds/Ping.aiff
			say "$phrase"
			sleep 3
		done
	done
}

